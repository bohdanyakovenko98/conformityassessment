﻿
namespace ConformityAssessment
{
    partial class Help
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(557, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Для визначення відповідності алгоритму до його проограмної реалізації необхідно:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(408, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "1. Завантажити програмний код на мові програмування С++:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(579, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "1.1 За домогою пункуту Головного меню Файл->Відкрити->Файл з програмним кодом.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(310, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "1.2 За домогою комбінації клавіш Ctrl+Shift+O.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(278, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "1.3 За допомогою текстового редактору.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 113);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(380, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "2. Завантажити зображення блок-схеми в форматі .json:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 169);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(278, 17);
            this.label7.TabIndex = 8;
            this.label7.Text = "2.3 За допомогою текстового редактору.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(30, 152);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(334, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "2.2 За домогою комбінації клавіш Ctrl+Alt+Shift+O.";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(30, 135);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(540, 17);
            this.label9.TabIndex = 6;
            this.label9.Text = "2.1 За домогою пункуту Головного меню Файл->Відкрити->Файл з блок-схемою.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 197);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(312, 17);
            this.label10.TabIndex = 9;
            this.label10.Text = "3. Виконати розпізнавання програмного коду:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(30, 236);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(274, 17);
            this.label11.TabIndex = 11;
            this.label11.Text = "3.2 За домогою комбінації клавіш Alt+F1.";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(30, 219);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(558, 17);
            this.label12.TabIndex = 10;
            this.label12.Text = "3.1 За домогою пункуту Головного меню Інструменти->Розпізнати програмний код.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(30, 303);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(274, 17);
            this.label13.TabIndex = 14;
            this.label13.Text = "4.2 За домогою комбінації клавіш Alt+F2.";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(30, 286);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(526, 17);
            this.label14.TabIndex = 13;
            this.label14.Text = "4.1 За домогою пункуту Головного меню Інструменти->Розпізнати блок-схему.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 264);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(269, 17);
            this.label15.TabIndex = 12;
            this.label15.Text = "4. Виконати розпізнавання блок-схеми:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(30, 368);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(274, 17);
            this.label16.TabIndex = 17;
            this.label16.Text = "5.2 За домогою комбінації клавіш Alt+F3.";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(30, 351);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(753, 17);
            this.label17.TabIndex = 16;
            this.label17.Text = "5.1 За домогою пункуту Головного меню Інструменти->Визначити відповідність програ" +
    "много коду та блок-схеми.";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(12, 329);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(387, 17);
            this.label18.TabIndex = 15;
            this.label18.Text = "5. Виконати порівняння програмного коду та блок-схеми:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(374, 411);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 18;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Help
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 446);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Help";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Довідка";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button1;
    }
}