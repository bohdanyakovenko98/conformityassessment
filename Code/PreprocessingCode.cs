﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConformityAssessment.Code
{
    class PreprocessingCode
    {
        private static readonly List<string> separators = new List<string>()//колекция разделителей
        { " ", "\n", "\r", ",", ".", "(", ")", "{", "}", "<", ">", "*",
              "/", "+", "-", "=", "&", "%", "?", "!", ";", ":", "^" };

        private static readonly List<string> KeyWorlds  = new List<string>()//колекция ключевых строк
        {"if", "for", "switch", "do", "break", "continue", "return", "while",
            "else", "case", "default"};

        public static string DeleteOneLineComments(string code)//функция удаление однострочных комметариев
        {
            try
            {
                int count;
                int index;
                for (int i = 0; i < code.Length - 1; i++)
                {
                    if (code[i] == '/' && code[i + 1] == '/')
                    {
                        count = 0;
                        index = i;
                        while (index < code.Length && code[index] != '\r')
                        {
                            count++;
                            index++;
                        }
                        code = code.Remove(i, count);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return code;
        }

        public static string DeleteMultiLineComments(string code)//функция удаление многострочных комметариев
        {
            try
            {
                int count;
                int index;
                for (int i = 0; i < code.Length - 1; i++)
                {
                    if (code[i] == '/' && code[i + 1] == '*')
                    {
                        count = 2;
                        index = i;
                        while (index < code.Length - 2 && (code[index] != '*' || code[index + 1] != '/'))
                        {
                            count++;
                            index++;
                        }
                        code = code.Remove(i, count);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return code;
        }

        public static string DeleteStringLiteral(string code)//aункция удаление строковых  ""
        {
            try
            {
                for (int i = 0; i < code.Length - 1; i++)
                {
                    int count;
                    int index;
                    if (code[i] == '\"')
                    {
                        count = 2;
                        index = i + 1;
                        while (index < code.Length - 1 && code[index] != '\"')
                        {
                            if (code[index] == '\\' && code[index + 1] == '\"')
                            {
                                count += 2;
                                index += 2;
                            }
                            else
                            {
                                count++;
                                index++;
                            }
                        }
                        code = code.Remove(i, count);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return code;
        }

        public static string DeleteSymvol(string code)//функция удаления символов ''
        {
            try
            {
                for (int i = 0; i < code.Length - 1; i++)
                {
                    if (code[i] == '\'')
                    {
                        if (code[i + 1] == '\\')
                        {
                            code = code.Remove(i, 4);
                            i += 4;
                        }
                        else
                        {
                            code = code.Remove(i, 3);
                            i += 3;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return code;
        }

        public static string DeleteParenthesis(string code)//aункция удаления выражения в круглых скобках ()
        {
            try
            {
                for (int i = 0; i < code.Length - 1; i++)
                {
                    if (code[i] == '(')
                    {
                        int index = i + 1;
                        int count = 0;
                        int counter = 1;
                        while (counter > 0)
                        {
                            if (code[index] == '(')
                            {
                                counter++;
                            }
                            else if (code[index] == ')')
                            {
                                counter--;
                            }
                            count++;
                            index++;
                        }
                        code = code.Remove(i + 1, count - 1);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return code;
        }

        public static List<string> AllocateTokens(string code)//функция выделения лексем
        {
            var tokens = new List<string>();

            try
            {
                string lexem = default;
                for (int i = 0; i < code.Length; i++)
                {
                    if (separators.Contains(code[i].ToString()))
                    {
                        if (!String.IsNullOrEmpty(lexem))
                        {
                            tokens.Add(lexem);
                            lexem = String.Empty;
                        }
                        tokens.Add(code[i].ToString());
                    }
                    else
                    {
                        lexem += code[i];
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return tokens;
        }

        public static string AllocateFirstTokens(string code)//функция выделения первой лексемы
        {
            string lexem = default;
            try
            {

                for (int i = 0; i < code.Length; i++)
                {
                    if (separators.Contains(code[i].ToString()))
                    {
                        if (!String.IsNullOrEmpty(lexem))
                        {
                            return KeyWorlds.Contains(lexem.ToLower()) ? lexem : "Process";
                        }
                    }
                    else
                    {
                        lexem += code[i];
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return KeyWorlds.Contains(lexem.ToLower()) ? lexem : "Process";
        }

    }
}
