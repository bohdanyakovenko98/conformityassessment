﻿
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ConformityAssessment.Code
{
    class LexicalAnalysis
    { 
        private string Code { get; set; }//програмный код функции на языке С++
        private List<string> Tokens { get; set; } = new List<string>();//лексемы програмного кода
        private List<string> Actions { get; set; } = new List<string>();//список действий
        

        private readonly List<string> Separators = new List<string>()//колекция разделителй
            { " ", "\n", "\r", ",", ".", "(", ")", "{", "}", "<", ">", "*",
              "/", "+", "-", "=", "&", "%", "?", "!", ";", ":", "^" };
        //список ключевых слов
        private Dictionary<string, int> KeyWorlds { get; set; } = new Dictionary<string, int>()//колекция ключевых слов
        {
            {"if", 1}, {"for", 1}, {"switch", 1},  {"do", 1},
            {"break", 2}, {"continue", 2}, {"return", 2}, 
            {"while", 3},
            {"else", 4},
            {"case", 5},{"default", 5}
        };
        
        //конструктор
        public LexicalAnalysis (string code)
        {
            Code = code;
        }

        //лексический анализ кода
        public (List<string>, string) Analysis()
        {
            string Name = default;
            try
            {
                //преддварительная обработка кода
                Code = PreprocessingCode.DeleteOneLineComments(Code);
                Code = PreprocessingCode.DeleteMultiLineComments(Code);
                Code = PreprocessingCode.DeleteSymvol(Code);
                Code = PreprocessingCode.DeleteStringLiteral(Code);
                Code = PreprocessingCode.DeleteParenthesis(Code);
                //выделяем лексемы
                Tokens = PreprocessingCode.AllocateTokens(Code);
                //удаляем лексемы типа ' '
                for (int i = Tokens.Count - 1; i >= 0; --i)
                    if (Tokens[i] == " " || Tokens[i] == "\r" || Tokens[i] == "\n") Tokens.RemoveAt(i);

                //находим имя функции
                Name = Tokens[Tokens.IndexOf("(") - 1];
                //обрабатываем тело функции
                BracesBody(Tokens.IndexOf("{"), false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return (Actions,Name);

        }
        //обработка тела в {}
        public int BracesBody(int index, bool flagcase)
        {
            try
            {
                //флаг первой лексемы
                bool f1 = true;

                //счетсчик для тела-ключевое слово
                int counter = 0;
                //идем полексемно
                for (int i = index + 1; i < Tokens.Count; i++)
                {
                    //если тело case
                    if (flagcase)
                    {
                        if (Tokens[i] == "}" || Tokens[i] == "case" || Tokens[i] == "default")
                        {
                            return i - 1;
                        }//если первая лексема не ключевое слово
                    }
                    else
                    //если конец тела
                    if (Tokens[i] == "}")
                    {
                        if (i != Tokens.Count - 1)
                        {
                            while (counter != 0) { Actions.Add("--"); counter--; }
                            Actions.Add("--");
                        }
                        return i;
                    }//если первая лексема не ключевое слово
                    if (f1 && !KeyWorlds.ContainsKey(Tokens[i]) && !Separators.Contains(Tokens[i]))
                    {
                        Actions.Add("Process");
                        f1 = false;
                    }
                    //если встретили клюевое слово
                    if (KeyWorlds.ContainsKey(Tokens[i]))
                    {
                        switch (KeyWorlds[Tokens[i]])
                        {
                            //если первый тип
                            case 1:
                                {
                                    Actions.Add(Tokens[i]);
                                    Actions.Add("++");
                                    i++;
                                    while (true)
                                    {
                                        if (Tokens[i] == ";")
                                        {
                                            Actions.Add("Process");
                                            Actions.Add("--");
                                            f1 = true;
                                            while (counter != 0) { Actions.Add("--"); counter--; }
                                            break;
                                        }
                                        else if (Tokens[i] == "{")
                                        {
                                            i = BracesBody(i, false);

                                            f1 = true;
                                            break;
                                        }
                                        else if (KeyWorlds.ContainsKey(Tokens[i]))
                                        {
                                            i--;
                                            counter++;
                                            break;
                                        }
                                        i++;
                                    }

                                    break;
                                }
                            //если второй тип
                            case 2:
                                {
                                    Actions.Add(Tokens[i]);
                                    f1 = true;
                                    while (true)
                                    {
                                        if (Tokens[i] == ";")
                                        {
                                            break;
                                        }
                                        i++;
                                    }
                                    break;
                                }
                            //если третий тип
                            case 3:
                                {
                                    if (Tokens[i + 3] != ";")
                                    {
                                        goto case 1;
                                    }
                                    Actions.Add(Tokens[i]);
                                    f1 = true;
                                    i += 3; ;
                                    break;
                                }
                            //если четвертый тип
                            case 4:
                                {
                                    if (Tokens[i + 1] != "if")
                                    {
                                        goto case 1;
                                    }
                                    Actions.Add(Tokens[i]);

                                    break;
                                }
                            case 5:
                                {
                                    int casebegin = Tokens.IndexOf(":", i);
                                    if (Tokens[casebegin + 1] == "{")
                                    { goto case 1; }

                                    Actions.Add(Tokens[i]);
                                    Actions.Add("++");
                                    i = BracesBody(casebegin, true);
                                    Actions.Add("--");
                                    break;
                                }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return 0;
        }


    }
}
