﻿using System;
using System.Windows.Forms;
using System.IO;


namespace ConformityAssessment
{
    public partial class Main : Form
    {
        Graph Graf1;
        Graph Graf2;

        public Main()
        {
            InitializeComponent();

            openFileDialog1.InitialDirectory = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), "..\\Example\\Code"));
            openFileDialog2.InitialDirectory = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), "..\\Example\\Flowchart"));
        }

        private void лексичнийАналізToolStripMenuItem_Click(object sender, EventArgs e)
        {
                Graf1 = new Graph();
                Graf1.BuildGrafFromCode(fastColoredTextBox1.Text);
               
        }

        private void файлЗПрограмнимКодомToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    using (StreamReader sr = new StreamReader(openFileDialog1.FileName))
                    {
                        fastColoredTextBox1.Text = sr.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        private void файлЗЗображеннямToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (openFileDialog2.ShowDialog() == DialogResult.OK)
                {

                    using (StreamReader sr = new StreamReader(openFileDialog2.FileName))
                    {
                        fastColoredTextBox2.Text = sr.ReadToEnd();
                    }
                    fastColoredTextBox2.Text = fastColoredTextBox2.Text.Replace("{", Environment.NewLine + "{" + Environment.NewLine);
                    fastColoredTextBox2.Text = fastColoredTextBox2.Text.Replace("}", Environment.NewLine + "}" + Environment.NewLine);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void розпізнатиБлоксхемуToolStripMenuItem_Click(object sender, EventArgs e)
        {
                Graf2 = new Graph();
                Graf2.BuildGrafFromFlowchart(fastColoredTextBox2.Text);
        }

        private void визначитиВідповідністьToolStripMenuItem_Click(object sender, EventArgs e)
        {
                MessageBox.Show("Відповідість алгоритму до його реалізації складає: " + Math.Round(Graph.GraphComparison(Graf1, Graf2), 2).ToString() + "%",
               "Результат", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void проПрограмуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.ShowDialog(Owner);
        }

        private void переглядДовідкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Help help = new Help();
            help.ShowDialog();
        }
    }
}
