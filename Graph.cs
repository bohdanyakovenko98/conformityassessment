﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.IO;
using ConformityAssessment.Code;
using ConformityAssessment.Flowchart;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace ConformityAssessment
{
    public class Graph
    {
        //Имя графа
       public string Name { get; set; }

        //список вершин графа
        public ArrayList Vertex { get; set; }

        //Список ребр графа
        public List<List<int>> Edge { get; set; }

        //конструктор
        public Graph()
        {
            Vertex = new ArrayList();
            Edge = new List<List<int>>();

            //Добавление вершины конца
            Vertex.Add("end");
            Edge.Add(new List<int>());

            //Добавление вершины начала
            Vertex.Add("begin");
            Edge.Add(new List<int>());
        }

        public void AddVertex(string str)
        {
            Vertex.Add(str);
            Edge.Add(new List<int>());
        }

        public void AddEdge(int start, int finish)
        {
            Edge[start].Add(finish);
        }

        public void BuildGrafFromCode(string code)
        {
            try
            {
                if (code.Length < 10)
                {
                    throw new Exception("Програмний код не введено!");
                }

                LexicalAnalysis lexicalAnalysis = new LexicalAnalysis(code);
                var resultLexicalAnalysis = lexicalAnalysis.Analysis();
                List<string> actions = resultLexicalAnalysis.Item1;
                Name = resultLexicalAnalysis.Item2;

                var stackTemp = new Stack<int>();//стек для хранения вершин, используеться операциями ++ и --
                var addEdgeNextVertex = new Stack<int>();//стек для хранения вершин, что имеют ребро с вершиной, что будет добавлена на следующем шаге
                var addEdgeRootVertex = new Stack<int>();//стек для хранения вершин, что являються корнем подграфа
                var stackBreak = new Stack<int>();//стек для хранения вершин, которые реализуют операцию break
                var stackContinue = new Stack<int>();// стек для хранения вершин, которые реализуют операцию continue
                var stackSwitch = new Stack<int>();// стек для хранения вершин case 

                bool f1 = false;//флаг, что используеться для обозначении вершины как корня подграфа
                bool f2 = false;//флаг, то используеться для запрета дуги в вершину, что будет добавлена

                int stackPop;//локальна переменная для изьятия вершины из stackTemp


                for (int i = 0; i < actions.Count; i++)//пока action ну будет пройден до конца
                {
                    if (actions[i] == "++")//операция ++
                    {
                        if (actions[i - 1] == "else") stackTemp.Push(-1);//если else, заносим вместо идекса вершины -1
                        else if (actions[i - 1] == "do") stackTemp.Push(-1 * Vertex.Count);//если do, заносим отреательный индекс вершины, что что будет добавлена
                        else stackTemp.Push(Vertex.Count - 1);//добавляем идекс текущей вершины

                        if (actions[i - 1] == "switch") f2 = true;//если вершина, что заноситься в стек switch, то запрещаем выводить из нее дугу
                    }
                    else if (actions[i] == "--")//если операция --
                    {

                        stackPop = stackTemp.Pop();//извлекам индеск вершины из stackTemp

                        if (stackPop == -1)//если извлекаем else
                        {
                            //добавляем последнюю вершину else к вершинам для соединения с корнем
                            if (!f2)
                            {
                                if (Edge[Edge.Count - 1].Count == 0) addEdgeRootVertex.Push(Edge.Count - 1);
                            }
                            else f2 = !f2;


                            f1 = true;//ведем дуги в корень

                            //если следущая операция -- и извлекаем вершину while или for то делаем ее корнем подграфа и выводим все дуги из addEdgeRootVertex
                            if (i < actions.Count - 1 && actions[i + 1] == "--"
                                && (Vertex[stackTemp.Peek()].ToString() == "while" || Vertex[stackTemp.Peek()].ToString() == "for"))
                            {
                                while (addEdgeRootVertex.Count != 0) this.AddEdge(addEdgeRootVertex.Pop(), stackTemp.Peek());
                                f1 = false;
                            }

                            f2 = true;
                        }
                        else if (stackPop < 0)//если извлекаем do
                        {
                            //добавляем новую вершину while
                            i++;
                            this.AddVertex(actions[i]);
                            this.AddEdge(Edge.Count - 2, Edge.Count - 1);
                            //добавляем ребро с while назад
                            this.AddEdge(Edge.Count - 1, stackPop * -1);
                            //добавляем ребро из вершины continue в вершину while
                            while (stackContinue.Count != 0) this.AddEdge(stackContinue.Pop(), Edge.Count - 1);
                            //добавляем вершину break к корню
                            while (stackBreak.Count != 0) addEdgeNextVertex.Push(stackBreak.Pop());
                        }
                        else switch (Vertex[stackPop])
                            {
                                case "while":
                                    {
                                        //если такое ребро уже есть то не дублируем его
                                        //добавляем ребро из последней вершины в вершину while
                                        if (!f2)
                                        {
                                            if (Edge[Edge.Count - 1].IndexOf(stackPop) == -1) this.AddEdge(Edge.Count - 1, stackPop);
                                        }
                                        else f2 = !f2;
                                        //добавляем ребро из вершины continue в вершину while
                                        while (stackContinue.Count != 0) this.AddEdge(stackContinue.Pop(), stackPop);
                                        //добавляем вершину while к вершинам для соединения с корнем
                                        addEdgeRootVertex.Push(stackPop);
                                        //добавляем вершину break к корню
                                        while (stackBreak.Count != 0) addEdgeRootVertex.Push(stackBreak.Pop());

                                        //рисуем ребра к корню
                                        f1 = true;
                                        //если следующаю вершину достаем while или for до рисуем в нее все корни
                                        if (i < actions.Count - 1 && actions[i + 1] == "--"
                                        && stackTemp.Count != 0 && (Vertex[stackTemp.Peek()].ToString() == "while" || Vertex[stackTemp.Peek()].ToString() == "for"))
                                        {
                                            while (addEdgeRootVertex.Count != 0) this.AddEdge(addEdgeRootVertex.Pop(), stackTemp.Peek());
                                            f1 = false;
                                        }
                                        f2 = true;
                                        break;
                                    }
                                case "for":
                                    {
                                        goto case "while";
                                    }
                                case "if":
                                    {
                                        //если следующая вершина else
                                        if (i < actions.Count - 1 && actions[i + 1] == "else")
                                        {
                                            //добавляем ребро из if в первую вершину else
                                            addEdgeNextVertex.Push(stackPop);
                                            //добавляем последнюю вершину if к вершинам для соединения с корнем
                                            if (!f2) addEdgeRootVertex.Push(Edge.Count - 1);
                                            else f2 = !f2;
                                        }
                                        else
                                        {
                                            //если конструкция без else
                                            //если с вершины не выходли ребра и нету флага неребра
                                            //добавляем последнюю вершину if к вершинам для соединения с корнем
                                            if (!f2)
                                            {
                                                if (Edge[Edge.Count - 1].Count == 0) addEdgeRootVertex.Push(Edge.Count - 1);
                                            }
                                            else f2 = !f2;

                                            //добавляем вершину if к вершинам для соединения с корнем
                                            addEdgeRootVertex.Push(stackPop);
                                            //рисуем ребра к корню
                                            f1 = true;
                                        }

                                        //если следующаю вершину достаем while или for до рисуем в нее все корни
                                        if (i < actions.Count - 1 && actions[i + 1] == "--"
                                        && stackTemp.Count != 0 && (Vertex[stackTemp.Peek()].ToString() == "while" || Vertex[stackTemp.Peek()].ToString() == "for"))
                                        {
                                            while (addEdgeRootVertex.Count != 0) this.AddEdge(addEdgeRootVertex.Pop(), stackTemp.Peek());
                                            f1 = false;
                                        }

                                        f2 = true;
                                        break;
                                    }

                                case "case":
                                    {
                                        //Добавляем вершину case для соединения с вершиной switch
                                        stackSwitch.Push(stackPop);
                                        //добавляем вершину break к корню
                                        while (stackBreak.Count != 0) addEdgeRootVertex.Push(stackBreak.Pop());


                                        break;
                                    }
                                case "switch":
                                    {
                                        while (stackSwitch.Count != 0) this.AddEdge(stackPop, stackSwitch.Pop());
                                        f1 = true;
                                        break;
                                    }

                                case "default":
                                    {
                                        goto case "case";
                                    }
                            }
                    }

                    //если вершина не do или else то добавляем ее вграф
                    else if (actions[i] != "do" && actions[i] != "else")
                    {
                        //добавляем следующую вершину
                        this.AddVertex(actions[i]);
                        //если флаг неребра
                        if (!f2)
                        {
                            //если он false, то соеденяем новую вершину с последней
                            this.AddEdge(Edge.Count - 2, Edge.Count - 1);
                        }
                        //если он true, то делаем его false
                        else f2 = !f2;

                        //если флаг корня
                        if (f1)
                        {
                            //добавляем в корень все вершины
                            while (addEdgeRootVertex.Count != 0) this.AddEdge(addEdgeRootVertex.Pop(), Edge.Count - 1);
                            f1 = !f1;
                        }
                        //добавляем все ребра для следующей вершины
                        while (addEdgeNextVertex.Count != 0) this.AddEdge(addEdgeNextVertex.Pop(), Edge.Count - 1);

                        if (actions[i] == "return")
                        {
                            this.AddEdge(Edge.Count - 1, 0);
                            f2 = true;

                        }
                        else if (actions[i] == "break")
                        {
                            stackBreak.Push(Edge.Count - 1);
                            f2 = true;
                        }
                        else if (actions[i] == "continue")
                        {
                            stackContinue.Push(Edge.Count - 1);
                            f2 = true;
                        }
                    }

                }

                if (f1)
                {
                    //добавляем в корень все вершины
                    while (addEdgeRootVertex.Count != 0) this.AddEdge(addEdgeRootVertex.Pop(), 0);
                    f1 = !f1;
                }
                if (!f2)
                {
                    //добавляем ребро из последней вершины в вершину end
                    this.AddEdge(Edge.Count - 1, 0);
                }
                else f2 = !f2;
                this.SaveToFile(Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), "..\\Example\\Graf1.txt")));
                MessageBox.Show("Розпізнавання програмного коду виконано!", "Програмний код", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //добавит стриговый возврат с результатом
        public void BuildGrafFromFlowchart(string code)//функция построения графа на основе блок схемы
        {
            try
            {
                if (code.Length < 10)
                {
                    throw new Exception("Невірний формат вхідних даних!");
                }
                int begin = 0;//индекс начала
                int end = 0;//индекс конца

                Stack<int> stackDelete = new Stack<int>();

                FlowchartModel model = JsonConvert.DeserializeObject<FlowchartModel>(code);//преобразуем код json в модель

                List<Blocks> blocks = model.Blocks.ToList();//приводим множество блоков в список

                if (blocks.Where(b => b.Type == "Начало / конец").Count() != 2) throw new Exception("Невірний формат вхідних даних!"); ;//если блоков начала/конца > 2

                for (int i = 0; i < blocks.Count; i++)//добавляем вершины в граф
                {
                    if (blocks[i].Type == "Начало / конец")
                    {
                        if (blocks[i].Text == "begin") begin = i;
                        else end = i;
                    }
                    else this.AddVertex(PreprocessingCode.AllocateFirstTokens(blocks[i].Text));
                }

                foreach (var arrow in model.Arrows)//добавляем дуги в граф
                {
                    if ((blocks[arrow.Begin].Type == "Ввод / вывод" && blocks[arrow.End].Type == "Блок") ||
                            (blocks[arrow.Begin].Type == "Блок" && blocks[arrow.End].Type == "Ввод / вывод"))
                    {
                        if (model.Arrows.Where(a => a.End == arrow.End).Count() == 1)
                        {
                            stackDelete.Push(arrow.Begin + 1);
                            stackDelete.Push(arrow.End + 1);
                        }
                    }
                    if (arrow.Begin == begin) AddEdge(1, arrow.End + 1);
                    else if (arrow.End == end) AddEdge(arrow.Begin + 1, 0);
                    else AddEdge(arrow.Begin + 1, arrow.End + 1);
                }

                while (stackDelete.Count != 0)
                {
                    end = stackDelete.Pop();
                    begin = stackDelete.Pop();
                    this.Edge[begin] = new List<int>(this.Edge[end]);
                    this.Edge.RemoveAt(end);
                    this.Vertex.RemoveAt(end);
                    for (int i = 0; i < Edge.Count; i++)
                    {
                        for (int j = 0; j < Edge[i].Count; j++)
                        {
                            if (Edge[i][j] >= end) Edge[i][j]--;
                        }
                    }
                }
                MessageBox.Show("Розпізнавання блок-схеми виконано!", "Блок-схема", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.SaveToFile(Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), "..\\Example\\Graf2.txt")));
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static double GraphComparison(Graph graf1, Graph graf2)//функция сравнения графов
        {
            bool[] used1 = default;
            bool[] used2 = default;

            try
            {
                if (graf1 == null || graf2 == null)
                {
                    throw new Exception("Невірний формат вхідних даних!");
                }
                Queue<int> q1 = new Queue<int>();//очередь для обхода в ширину графа graf1
                Queue<int> q2 = new Queue<int>();//очередь для обхода в ширину графа graf2

                int cur1 = 0;//перменная для хранения вершины q1
                int cur2 = 0;//переменная для хранения вершины q2

                used1 = Enumerable.Repeat<bool>(false, graf1.Vertex.Count).ToArray();//посещенность вершин graf1
                used2 = Enumerable.Repeat<bool>(false, graf2.Vertex.Count).ToArray();//посещенность вершин graf2

                used1[1] = used2[1] = used1[0] = used2[0] = true;

                q1.Enqueue(1);
                q2.Enqueue(1);

                while (q1.Count != 0 && q2.Count != 0)
                {
                    if (q1.Count != 0) cur1 = q1.Dequeue();
                    if (q2.Count != 0) cur2 = q2.Dequeue();


                    if (graf1.Vertex[cur1].Equals(graf2.Vertex[cur2]))
                    {
                        used1[cur1] = true;
                        used2[cur2] = true;
                    }

                    for (int i = 0; i < graf1.Edge[cur1].Count; i++) if (used1[graf1.Edge[cur1][i]] != true) q1.Enqueue(graf1.Edge[cur1][i]);
                    for (int i = 0; i < graf2.Edge[cur2].Count; i++) if (used2[graf2.Edge[cur2][i]] != true) q2.Enqueue(graf2.Edge[cur2][i]);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return (double)(used1.Where(x => x).Count() + used2.Where(x => x == true).Count() - 2) / (double)(used1.Length + used2.Length - 2) * 100;
        }

        public void SaveToFile(string path)
        {
            string str;
            using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
            {
                for (int i = 0; i < Edge.Count; i++)
                {
                    str = String.Empty;
                    str+= i.ToString() + " " + Vertex[i] +  " - ";
                    for (int j = 0; j < Edge[i].Count; j++)
                    {
                        str += Edge[i][j].ToString() + " "; 
                    }
                    sw.WriteLine(str);
                }
                
            }
           

        }

    }
}
