﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ConformityAssessment.Flowchart
{
    [JsonObject]
    public class FlowchartModel
    {
        [JsonProperty("blocks")]
        public IEnumerable<Blocks> Blocks { get; set; }

        [JsonProperty("arrows")]
        public IEnumerable<Arrows> Arrows { get; set; }

    }

    [JsonObject]
    public class Blocks
    {
        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("type")]
        public  string Type { get; set; }
    }

    [JsonObject]
    public class Arrows
    {
        [JsonProperty("startIndex")]
        public int Begin { get; set; }

        [JsonProperty("endIndex")]
        public int End { get; set; }
    }
}
